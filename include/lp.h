/* -*- mode: c++ -*- */
#pragma once

#include <cassert>

#include <arrayfire.h>

class LinearProgram
{
public:

  enum class ConstraintType
  {
    LE,  /** Less than or equal to */
    EQ,  /** Equal to */
    GE   /** Greater than or equal to */
  };

  enum class Optimization
  {
    MINIMIZE,
    MAXIMIZE
  };

  enum class Feasibility
  {
    INFEASIBLE,
    OPTIMAL,
    SUBOPTIMAL
  };

  struct SolveResult
  {
    Feasibility feasibility = Feasibility::INFEASIBLE;
    std::vector<float> solution;
  };

  LinearProgram( const af::array& objFn, Optimization opType = Optimization::MAXIMIZE )
    : mOptimizationType(opType),
      mNumVars(objFn.dims()[1])
  {
    assert(opType == Optimization::MAXIMIZE); // MINIMIZE not implemented yet

    // Objective function is entered in the form (c1x + c2x + ... = P )
    // but needs to be entered as equal to 0, with positive P on the lhs.
    //
    // Create initial tableau w/ objective fn ( 0 = -c1x + -c2x + ... + P )
    mTableau = af::join( 1,
			 af::constant(0, 1, 1),
			 -1 * objFn,
			 af::constant(1, 1, 1)
			);
  }

  LinearProgram( const float* objFn, int numVars, Optimization opType = Optimization::MAXIMIZE )
    : LinearProgram( af::array(1, numVars, objFn), opType ) {}
  
  void addConstraint( const af::array& constraint, float rhs,
		      ConstraintType type = ConstraintType::LE );

  void addConstraint( const float* constraint, int numVars,  float rhs,
		      ConstraintType type = ConstraintType::LE )
  {
    addConstraint( af::array(1, numVars, constraint), rhs, type );
  }
  
  LinearProgram::SolveResult solve();

private:

  

  /**
   * The tableau is a matrix used for in-place solving of the Linear Program.
   *
   * The tableau will be of the form:
   *
   *   [ 0  | -o1 -o2 -o3  P  0   0  ]
   *   [ -- | ---------------------- ]
   *   [ b1 | c1  c2   c3  0  s1  0  ]
   *   [ b2 | d1  d2   d3  0  0   s2 ]
   *   [    |     .                  ]
   *   [    |     .                  ]
   *
   *   ox: coefficients of objective function
   *   P : the optimization function value (what we're trying to maximize)
   *
   *   cx: coefficients of constraint 'c'
   *   dx: coefficients of constraint 'd'
   *   sx: slack variables introduced to turn inequalities into equalities
   *
   *   bx: the values for each of the constraints
   *
   * Where the first column is the value of the equalities defined by the problem constraints
   * and the first row is the objective function to maximize.
   */
  af::array mTableau;

  int getPivotColumn();
  int getPivotRow( int pivotColumn );
  void pivot( int pRow, int pCol );
  std::vector<float> interpret();

  bool isSolved();

  Optimization mOptimizationType;
  int mNumVars;
};
