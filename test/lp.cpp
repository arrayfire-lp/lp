#include <algorithm>

#include <gtest/gtest.h>
#include <arrayfire.h>

#include <stdio.h>
#include <iostream>

#include "lp.h"

/**
 * Tests solving a maximization problem with two variables.
 *   Problem Source: https://people.richland.edu/james/ictcm/2006/simplex.html 
 */
TEST( LinearProgramTest, TwoVarSolve )
{
  std::vector<float> expected_solution = { 6, 3 };
  
  LinearProgram lp( (const float[]) {40, 30}, 2 );
  lp.addConstraint( (const float[]) {1,  2 }, 2, 16 );
  lp.addConstraint( (const float[]) {1,  1 }, 2, 9  );
  lp.addConstraint( (const float[]) {3,  2 }, 2, 24 );

  LinearProgram::SolveResult result = lp.solve();

  ASSERT_EQ( result.feasibility, LinearProgram::Feasibility::OPTIMAL );
  for (int i=0; i < result.solution.size(); ++i)
  {
    ASSERT_FLOAT_EQ( result.solution[i], expected_solution[i] );
  }
}

/**
 * Tests solving a maximization problem with three variables.
 *   Problem Source: https://www.youtube.com/watch?v=gRgsT9BB5-8
 */
TEST( LinearProgramTest, ThreeVarSolve )
{
  std::vector<float> expected_solution = { 200, 0, 300 };

  LinearProgram lp( (const float[]) {7, 8, 10}, 3 );
  lp.addConstraint( (const float[]) {2, 3, 2 }, 3, 1000 );
  lp.addConstraint( (const float[]) {1, 1, 2 }, 3, 800  );

  LinearProgram::SolveResult result = lp.solve();

  ASSERT_EQ( result.feasibility, LinearProgram::Feasibility::OPTIMAL );
  ASSERT_EQ( result.solution, expected_solution );
}
