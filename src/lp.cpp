#include <cassert>
#include <limits>

#include <arrayfire.h>

#include "lp.h"

void LinearProgram::addConstraint( const af::array& constraint, float rhs, ConstraintType type )
{
  // Not implemented yet
  assert( type != ConstraintType::EQ );
  assert( type != ConstraintType::GE );

  if (type == ConstraintType::LE)
  {
    // Create constraint row
    //   [ rhs c1 c2 ... s1 s2 ... ]
    // rhs and slack variables need to be added
    int ncols = mTableau.dims()[1];

    int constraint_cols = constraint.dims()[1];
    int needed_cols = ncols - constraint_cols - 1;

    af::array rhs_col = af::constant(rhs, 1, 1);
    af::array extra_coefficients = af::constant(0, 1, needed_cols);
    af::array resized_constraint = af::join(1, rhs_col, constraint, extra_coefficients);

    mTableau = af::join(0, mTableau, resized_constraint);

    // Add new slack variable to the tableau
    int nrows = mTableau.dims()[0];
    af::array new_slack_var = af::constant(0, nrows, 1);
    new_slack_var( nrows-1 ) = 1;

    mTableau = af::join(1, mTableau, new_slack_var);
  }
}

LinearProgram::SolveResult LinearProgram::solve()
{
  SolveResult result;

  while ( ! isSolved() )
  {
    int p_col = getPivotColumn();
    int p_row = getPivotRow( p_col );

    if ( p_row == -1 || p_col == -1 )
    {
      result.solution = std::vector<float>(mNumVars, -1.0);
      result.feasibility = Feasibility::INFEASIBLE;
      return result;
    }

    pivot( p_row, p_col );
  }

  result.solution = interpret();
  result.feasibility = Feasibility::OPTIMAL;
  return result;
}

std::vector<float> LinearProgram::interpret()
{
  std::vector<float> solution(mNumVars, 0);

  for(int col_idx=1; col_idx <= mNumVars; ++col_idx) // Skip value column
  {
    af::array var_col = mTableau.col(col_idx).rows(1, af::end); // Skip objective row
    bool is_var_basic = af::count<unsigned>( var_col ) == 1; // Count non-zero elements

    if ( is_var_basic )
    {
      float max_val = std::numeric_limits<double>::infinity();
      unsigned row_idx = std::numeric_limits<unsigned>::max();

      af::max( &max_val, &row_idx, var_col );

      solution[col_idx - 1] = mTableau(row_idx + 1, 0).scalar<float>();
    }
  }

  return solution;
}

int LinearProgram::getPivotColumn()
{
  af::array min_val(1, 1, (const float[]) {-1});
  af::array idx(1, 1, (const float[]) {-1});

  // Coefficients of objective function are the first row without the first column
  af::min( min_val, idx, mTableau.row(0).cols(1, af::end) );

  return idx.scalar<unsigned>() + 1; // Account for shifting cols over by one (to remove values)
}

int LinearProgram::getPivotRow( int pivotColumn )
{
  af::array ratios = mTableau.col(0).rows(1, af::end) /
    mTableau.col(pivotColumn).rows(1, af::end);

  // Infeasibility condition - all calculated ratios are negative
  if ( af::allTrue<bool>( ratios < 0 ) )
  {
    return -1;
  }

  float min_val = std::numeric_limits<double>::infinity();
  unsigned idx = std::numeric_limits<unsigned>::max();

  af::min( &min_val, &idx, ratios );

  return idx + 1; // Account for shifting rows over by one (to remove obj fn)
}

void LinearProgram::pivot( int pRow, int pCol )
{
  // Divide pivot row by the pivot value to make pivot value == 1
  float pivot_value = mTableau( pRow, pCol ).scalar<float>();
  mTableau.row( pRow ) /= pivot_value;

  // Perform gaussian elimination to zero out the variable in other rows
  int nrows = mTableau.dims()[0];
  for( int row=0; row < nrows; ++row) // TODO: Switch this to faster af::gfor loop
  {
    if ( row != pRow )
    {
      float coefficient = 0 - mTableau(row, pCol).scalar<float>();
      af::array reduction_row = coefficient * mTableau.row( pRow );

      mTableau.row(row) += reduction_row;
    }
  }
}

bool LinearProgram::isSolved()
{
  return ! af::anyTrue<bool>(mTableau.row(0) < 0);
}
