###########################################################################
#
###########################################################################


##################################################
# Globals
##################################################
CMAKE_MINIMUM_REQUIRED( VERSION 3.0 )
PROJECT( ArrayFire-Linear-Programming )

SET( LIB_NAME   af_lp )

SET( ROOT_DIR    ${PROJECT_SOURCE_DIR} )
SET( SRC_DIR     ${ROOT_DIR}/src )
SET( TEST_DIR    ${ROOT_DIR}/test )
SET( INCLUDE_DIR ${ROOT_DIR}/include )
SET( BIN_DIR     ${ROOT_DIR}/bin )

##################################################
# Dependencies
##################################################

FIND_PACKAGE( ArrayFire REQUIRED )

##################################################
# Build Library
##################################################
ADD_LIBRARY( ${LIB_NAME} SHARED ${SRC_DIR}/lp.cpp )

TARGET_INCLUDE_DIRECTORIES( ${LIB_NAME} PUBLIC
  ${INCLUDE_DIR}
  ${ArrayFire_INCLUDE_DIRS}
  )

TARGET_LINK_LIBRARIES( ${LIB_NAME} ${ArrayFire_LIBRARIES} )

SET_TARGET_PROPERTIES( ${LIB_NAME} PROPERTIES
  FRAMEWORK TRUE
  PUBLIC_HEADER ${INCLUDE_DIR}/lp.h
  )

##################################################
# Build Tests
##################################################
FIND_PACKAGE( GTest )

IF( ${GTEST_FOUND} )
  ENABLE_TESTING()

  ADD_EXECUTABLE( af_lp_test ${TEST_DIR}/lp.cpp )

  TARGET_INCLUDE_DIRECTORIES( af_lp_test PUBLIC
    ${INCLUDE_DIR}
    ${ArrayFire_INCLUDE_DIRS}
    ${GTEST_INCLUDE_DIRS}
    )

  TARGET_LINK_LIBRARIES( af_lp_test
    ${LIB_NAME}
    ${GTEST_BOTH_LIBRARIES}
    ${ArrayFire_LIBRARIES}
    )

  ADD_TEST( LibraryTest af_lp_test )

ENDIF()

##################################################
# Install Library
##################################################
INSTALL( TARGETS ${LIB_NAME}
  PUBLIC_HEADER DESTINATION include
  LIBRARY DESTINATION lib
  )
