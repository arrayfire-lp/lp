# ArrayFire LP

ArrayFire LP is a [Linear Programming](https://en.wikipedia.org/wiki/Linear_programming) problem solver library written using the [ArrayFire](https://arrayfire.org) library. The library uses an implementation of the simplex method in order to solve the LP. See the _Simplex Algorithm Resources_ section at the bottom of this page for more information on how the simplex algorithm works.


# Building

ArrayFire LP uses the CMake build system and can be built as follows:

``` bash
  >> mkdir build
  >> cd build
  >> cmake ..
  >> cmake --build .
```


## Requirements

The `arrayfire-lp` library requires:
  - a c++ compiler with c++11 support. `gcc 6.1.1` was used during initial development
  - `cmake` version 3.0 or higher
  - `ArrayFire` version 3.4.1 or higher


## Development Docker Image

Optionally, the docker image specified in the `docker` directory can be used to build the library to avoid needing to install specific version of requirements yourself.

1. Build the docker image from the provided Dockerfile

     ``` bash
       >> docker build --tag=arrayfire-lp-dev ./docker/Dockerfile
     ```

2. Build and ArrayFire-LP library itself

     ``` bash
       >> sudo docker run --rm -v $(pwd):/code -w /code arrayfire-lp-dev /bin/bash -c 'mkdir -p build && cd build && cmake .. && cmake --build .'
     ```

# Simplex Algorithm Resources

Resources:
  - [https://people.richland.edu/james/ictcm/2006/simplex.html][]
  - [https://www.youtube.com/watch?v=gRgsT9BB5-8][]
